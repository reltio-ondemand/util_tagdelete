/**
 * 
 */
package com.reltio.rocs.util;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shivaputra Patil
 *
 */
public abstract class LineNumberReaderHelper {

	public static List<String> readLines(int size, BufferedReader reader) throws Exception {

		List<String> result = new ArrayList<>(size);

		String line = "";

		while (size > result.size() && (line = reader.readLine()) != null) {

			if (line.length() != 0 && !line.startsWith("#")) {
				result.add(line);
			}
		}
		
		return result;
	}
}
