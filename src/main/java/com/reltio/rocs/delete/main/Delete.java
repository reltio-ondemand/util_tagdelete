package com.reltio.rocs.delete.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import com.reltio.cst.util.Util;
import com.reltio.rocs.delete.dto.ScanResponseURI;
import com.reltio.rocs.delete.dto.Uri;
import com.reltio.rocs.delete.executor.DeleleteEntityByCrossWalkExecutor;
import com.reltio.rocs.delete.executor.DeleteEntityExecutor;
import com.reltio.rocs.util.LineNumberReaderHelper;

public class Delete {
	private static final Logger LOGGER = LogManager.getLogger(Delete.class.getName());
	private static final Logger logPerformance = LogManager.getLogger("performance-log");
	private int noOfRecordsPerCall = 100;

	private TokenGeneratorService tokenGeneratorService;

	public TokenGeneratorService getTokenGeneratorService() {
		return tokenGeneratorService;
	}

	public void setTokenGeneratorService(TokenGeneratorService tokenGeneratorService) {
		this.tokenGeneratorService = tokenGeneratorService;
	}

	public static void main(String[] args) throws Exception {
		LOGGER.info("Process Started...");

		Properties properties = new Properties();
		String propertyFilePath = "configuration.properties";
		Util.setHttpProxy(properties);

		if (args.length != 0) {
			propertyFilePath = args[0];
		}

		LOGGER.info("Processing with configuration file = " + propertyFilePath);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Processing with configuration file = " + propertyFilePath);
		}
		
		properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

		
		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("ACTION", "ENVIRONMENT_URL", "INPUT", "DELIMITER", "TENANT_ID", "AUTH_URL","OUTPUT_FILE"));

        if (!missingKeys.isEmpty()) {
            System.out.println("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }

		String outputFile = properties.getProperty("OUTPUT_FILE");
		Delete util = initialize(properties);
		final ReltioAPIService restApi = Util.getReltioService(properties);

	
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		String action = properties.getProperty("ACTION");
		String delimitor = properties.getProperty("DELIMITER");
		
		
		

		switch (action) {

		case "filter":
			util.deleteByFilter(properties, writer, restApi);
			break;

		case "sourcesystem":
			util.deleteByFilter(properties, writer, restApi);
			break;

		case "DeleteAll":
			util.deleteByFilter(properties, writer, restApi);
			break;

		case "DeleteSingleTag":
			util.deleteByFilter(properties, writer, restApi);
			break;

		case "uris":
			util.deleteByUriList(properties, writer, restApi);
			break;

		case "crosswalks":
			util.deleteByEntitiesByCrossWalks(properties, writer, restApi, delimitor);
			break;

		case "Addtag":
			util.addTags(properties, writer, restApi, delimitor, true);
			break;

		default:
			LOGGER.info("options is not in avaliable action = " + action);
		}
		LOGGER.info("Process Ended..");
	}

	/**
	 * This will delete entities by thier cross walks
	 */
	protected void deleteByEntitiesByCrossWalks(Properties config, BufferedWriter writer, ReltioAPIService restApi,
			String dili) throws Exception {

		int threadCount = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
		long processedCount = 0l;
		final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
		long programStartTime = System.currentTimeMillis();
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);
		
		final String tenant_id = config.getProperty("TENANT_ID");
		final String environment_url = config.getProperty("ENVIRONMENT_URL");
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/";
		
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

		writer.write("Deleted from content of the file as uris = " + config.getProperty("INPUT"));
		writer.newLine();
		writer.flush();

		FileReader reader = new FileReader(config.getProperty("INPUT"));
		LineNumberReader buffer = new LineNumberReader(reader);
		List<String> sourceInfos = LineNumberReaderHelper.readLines(10, buffer);

		while (sourceInfos.size() != 0) {

			DeleleteEntityByCrossWalkExecutor executor = new DeleleteEntityByCrossWalkExecutor(writer, restApi,
					tenant_url, sourceInfos, dili);
			futures.add(executorService.submit(executor));
			waitForTasksReady(futures, 0);
			sourceInfos = LineNumberReaderHelper.readLines(5, buffer);
		}

		processedCount+=waitForTasksReady(futures, threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		if (processedCount > 0) {
			printPerformanceLog(executorService.getCompletedTaskCount() * noOfRecordsPerCall,
					processedCount, programStartTime, threadCount);
		}
		executorService.shutdown();
		Util.close(buffer);
	}

	protected void addTags(Properties config, BufferedWriter writer, ReltioAPIService restApi, String dili,
			Boolean load) throws Exception {

		int threadCount = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
		long processedCount = 0l;
		final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
		long programStartTime = System.currentTimeMillis();
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);
		final String tenant_id = config.getProperty("TENANT_ID");
		final String environment_url = config.getProperty("ENVIRONMENT_URL");
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/";
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

		writer.write("addTags from content of the file as uris = " + config.getProperty("INPUT"));
		writer.newLine();
		writer.flush();

		FileReader reader = new FileReader(config.getProperty("INPUT"));
		LineNumberReader buffer = new LineNumberReader(reader);
		List<String> sourceInfos = LineNumberReaderHelper.readLines(10, buffer);

		while (sourceInfos.size() != 0) {

			DeleleteEntityByCrossWalkExecutor executor = new DeleleteEntityByCrossWalkExecutor(writer, restApi,
					tenant_url, sourceInfos, dili, load);
			futures.add(executorService.submit(executor));
			waitForTasksReady(futures, 0);
			sourceInfos = LineNumberReaderHelper.readLines(5, buffer);
		}

		processedCount+=waitForTasksReady(futures, threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		if (processedCount > 0) {
			printPerformanceLog(executorService.getCompletedTaskCount() * noOfRecordsPerCall,
					processedCount, programStartTime, threadCount);
		}
		executorService.shutdown();
		Util.close(buffer);
	}

	/**
	 * This will validate credentials
	 */
	private static Delete initialize(Properties properties) {

		Delete util = new Delete();
		try {

			String username = properties.getProperty("USERNAME");
			String password = properties.getProperty("PASSWORD");
			String authUrl = properties.getProperty("AUTH_URL");
			util.setTokenGeneratorService(new TokenGeneratorServiceImpl(username, password, authUrl));

		} catch (APICallFailureException e) {
			LOGGER.error("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
			System.exit(0);
		} catch (GenericException e) {
			LOGGER.error(e.getExceptionMessage());
			System.exit(0);
		}
		return util;
	}

	/**
	 * This will delete entities by URI mentioned in file
	 * 
	 * @throws Exception
	 */
	protected void deleteByUriList(Properties config, BufferedWriter writer, ReltioAPIService restApi)
			throws Exception {
		
		
		int threadCount = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
		long processedCount = 0l;
		final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
		long programStartTime = System.currentTimeMillis();
		// ExecutorService executorService =
		// Executors.newFixedThreadPool(threadCount);
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);

		final String tenant_id = config.getProperty("TENANT_ID");
		final String environment_url = config.getProperty("ENVIRONMENT_URL");
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/";
		Gson gson = new Gson();
		//int threadsNumber = 100;
		//ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
		writer.write("Deleted from content of the file as uris = " + config.getProperty("INPUT"));
		writer.newLine();
		writer.flush();

		FileReader reader = new FileReader(config.getProperty("INPUT"));
		BufferedReader buffer = new BufferedReader(reader);

		List<String> uris = LineNumberReaderHelper.readLines(10, buffer);

		while (uris.size() != 0) {

			DeleteEntityExecutor executor = new DeleteEntityExecutor(writer, restApi, tenant_url, uris, gson);
			futures.add(executorService.submit(executor));

			waitForTasksReady(futures, 0);
			uris = LineNumberReaderHelper.readLines(10, buffer);
		}

		processedCount+=waitForTasksReady(futures, threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));

		if (processedCount > 0) {
			printPerformanceLog(executorService.getCompletedTaskCount() * noOfRecordsPerCall,
					processedCount, programStartTime, threadCount);
		}
		executorService.shutdown();

		Util.close(buffer, reader);
	}

	/**
	 * This will scan the records either based on source and entity type or by
	 * filter specified in property configuration.
	 * 
	 */
	protected void deleteByFilter(Properties config, BufferedWriter writer, ReltioAPIService restApi) throws Exception {

		int threadCount = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
		long processedCount = 0l;
		final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
		long programStartTime = System.currentTimeMillis();
		// ExecutorService executorService =
		// Executors.newFixedThreadPool(threadCount);
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);

		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
		Gson gson = new Gson();
		boolean isDone = false;
		String intialJSON = "";
		// String apiUrl = config.getProperty("API_URL");
		final String tenant_id = config.getProperty("TENANT_ID");
		final String environment_url = config.getProperty("ENVIRONMENT_URL");
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/";

		final String totalUrl = getSearchUrl(config, "total");
		String totalscanUrlResponse = restApi.get(totalUrl);
		LOGGER.info(totalscanUrlResponse);

		final String scanUrl = getSearchUrl(config, "scan");
		writer.write("Scan url for entities = " + scanUrl);
		writer.newLine();
		writer.flush();

		LOGGER.info("Scan url for entities = " + scanUrl);

		while (!isDone) {

			for (int i = 0; i < threadCount; i++) {

				String scanResponse = restApi.post(scanUrl, intialJSON);

				if (!Util.isEmpty(scanResponse)) {

					ScanResponseURI scanResponseObjUri = gson.fromJson(scanResponse, ScanResponseURI.class);
					final List<Uri> uris = scanResponseObjUri.getObjects();

					if (Util.isNotEmpty(uris)) {
						DeleteEntityExecutor executor = new DeleteEntityExecutor(writer, uris, restApi, tenant_url);
						futures.add(executorService.submit(executor));
					} else {
						isDone = true;
						break;
					}
					scanResponseObjUri.setObjects(null);
					intialJSON = gson.toJson(scanResponseObjUri.getCursor());
					intialJSON = "{\"cursor\":" + intialJSON + "}";
				}
			}

		}
		processedCount += waitForTasksReady(futures, threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		
		if (processedCount > 0) {
			printPerformanceLog(executorService.getCompletedTaskCount() * noOfRecordsPerCall,
					processedCount, programStartTime, threadCount);
		}
		
		executorService.shutdown();
	}

	/**
	 * This method will returns either total URL or scan URL based on the
	 * properties mentioned in configuration file
	 */
	protected String getSearchUrl(Properties config, String type) {

		String url = "";
		final String tenant_id = config.getProperty("TENANT_ID");
		final String environment_url = config.getProperty("ENVIRONMENT_URL");
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/";

		if ("scan".equals(type)) {

			if (config.getProperty("INPUT") != null) {
				url = tenant_url + "entities/_scan?filter=" + config.getProperty("INPUT") + "&select=uri,tags&max=200";
			}

		} else if ("total".equals(type)) {

			if (config.getProperty("INPUT") != null) {
				url = tenant_url + "entities/_total?filter=" + config.getProperty("INPUT") + "&select=uri";
			}
		}

		return url;
	}

	/**
	 * This will remove completed tasks in collection;
	 */
	/*
	 * public void waitForTasksReady(Collection<Future<Long>> futures, int
	 * maxNumberInList) throws Exception {
	 * 
	 * while (futures.size() > maxNumberInList) { Thread.sleep(20);
	 * 
	 * for (Future<Long> future : new ArrayList<Future<Long>>(futures)) { if
	 * (future.isDone()) { futures.remove(future); } } } }
	 */

	public static long waitForTasksReady(Collection<Future<Long>> futures, int maxNumberInList) {
		long totalResult = 0l;
		while (futures.size() > maxNumberInList) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				// ignore it...
			}
			for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
				if (future.isDone()) {
					try {
						totalResult += future.get();
						futures.remove(future);
					} catch (Exception e) {
						LOGGER.error(e.getMessage());
						LOGGER.debug(e);
					}
				}
			}
		}
		return totalResult;
	}

	public static void printPerformanceLog(long totalTasksExecuted, long totalTasksExecutionTime, long programStartTime,
			long numberOfThreads) {
		LOGGER.info("[Performance]: ============= Current performance status (" + new Date().toString()
				+ ") =============");
		long finalTime = System.currentTimeMillis() - programStartTime;
		LOGGER.info("[Performance]:  Total processing time : " + finalTime);

		LOGGER.info("[Performance]:  total task executed : " + totalTasksExecuted);
		LOGGER.info("[Performance]:  Total OPS (Match Pairs extracted for Entities / Time spent from program start): "
				+ (totalTasksExecuted / (finalTime / 1000f)));
		LOGGER.info(
				"[Performance]:  Total OPS without waiting for queue (Match Pairs extracted for Entities / (Time spent from program start - Time spent in waiting for API queue)): "
						+ (totalTasksExecuted / ((finalTime) / 1000f)));
		LOGGER.info(
				"[Performance]:  API Server data load requests OPS (Match Pairs extracted for Entities / (Sum of time spend by API requests / Threads count)): "
						+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		LOGGER.info(
				"[Performance]: ===============================================================================================================");

		// log performance only in separate logs
		logPerformance.info("[Performance]: ============= Current performance status (" + new Date().toString()
				+ ") =============");
		logPerformance.info("[Performance]:  Total processing time : " + finalTime);

		logPerformance.info("[Performance]:  Entities sent: " + totalTasksExecuted);
		logPerformance
				.info("[Performance]:  Total OPS (Match Pairs extracted for Entities / Time spent from program start): "
						+ (totalTasksExecuted / (finalTime / 1000f)));
		logPerformance
				.info("[Performance]:  Total OPS without waiting for queue (Match Pairs extracted for Entities / (Time spent from program start - Time spent in waiting for API queue)): "
						+ (totalTasksExecuted / ((finalTime) / 1000f)));
		logPerformance
				.info("[Performance]:  API Server data load requests OPS (Match Pairs extracted for Entities / (Sum of time spend by API requests / Threads count)): "
						+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		logPerformance.info(
				"[Performance]: ===============================================================================================================");
	}
}
