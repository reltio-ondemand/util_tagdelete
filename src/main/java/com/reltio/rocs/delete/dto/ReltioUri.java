package com.reltio.rocs.delete.dto;

public class ReltioUri {
	
	private String uri;
	
	private ReltioEntity object;
	
	private boolean successful;
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public ReltioEntity getObject() {
		return object;
	}
	
	public void setObject(ReltioEntity object) {
		this.object = object;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReltioUri [uri=").append(uri).append(", object=").append(object).append(", successful=")
				.append(successful).append("]");
		return builder.toString();
	}
}
