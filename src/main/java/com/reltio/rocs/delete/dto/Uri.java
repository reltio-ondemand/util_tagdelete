package com.reltio.rocs.delete.dto;

import java.util.Collections;
import java.util.List;

public class Uri {
	
	private String uri;
	private List<String> tags=Collections.EMPTY_LIST;

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "Uri [uri=" + uri + ", tags=" + tags + "]";
	}
}