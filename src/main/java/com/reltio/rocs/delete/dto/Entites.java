package com.reltio.rocs.delete.dto;

import com.google.gson.annotations.SerializedName;

public class Entites {

	int index;

	@SerializedName("object")
	private Entity entitiy;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Entity getEntitiy() {
		return entitiy;
	}

	public void setEntitiy(Entity entitiy) {
		this.entitiy = entitiy;
	}

}
