/**
 * * @author spatil
 * This callable executor which delete entities
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Entity;
import com.reltio.rocs.delete.dto.Uri;

/**
 * This class is call able instance with help of the ReltioAPIService it deletes
 * entities passed in uris and uriList
 */
public class DeleteEntityExecutor implements Callable<Long> {

	private BufferedWriter writer;
	private List<Uri> uris = Collections.emptyList();
	private List<String> uriList = Collections.emptyList();
	private ReltioAPIService restApi;
	private Gson gson;
	private String apiUrl;

	public DeleteEntityExecutor(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi, String apiUrl) {

		this.writer = writer;
		this.uris = uris;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
	}

	public DeleteEntityExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl, List<String> uriList, Gson gson) {

		this.writer = writer;
		this.uriList = uriList;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
		this.gson = gson;
	}

	@Override
	/**
	 * @param writer
	 * @param count
	 * @param uris
	 * @param restApi
	 * @param apiUrl
	 * 
	 * @return {@link Callable} returns that can be submitted to executor
	 */
	public Long call() throws Exception {

		for (Uri uri : uris) {
			List<String> tags=uri.getTags();
			for (String tag :tags) {											
				String resp = restApi.delete(apiUrl + uri.getUri()+"/tags?element="+tag, "");
				writer.write(tag + "--" + resp+"\n");
				writer.flush();
			}
		}
		
		for (String uri : uriList) {
			String resp = restApi.get(apiUrl + uri+"?select=uri,tags");
			try {
				Entity entity = gson.fromJson(resp, Entity.class);
				List<String> tags=entity.getTags();
				for (String tag :tags) {
					String deleteRessponse = restApi.delete(apiUrl + entity.getUri()+"/tags?element="+URLEncoder.encode(tag, "UTF-8"), "");
					writer.write(tag + " -- " + deleteRessponse+"\n");
					writer.flush();
				}
				
			} catch (Exception e) {
				System.out.println("failed to delete " + uri + e.getMessage());
			}
		}

		System.out.println("deleted Tagged records count = " + uris.size());
		System.out.println("deleted Tagged records count = " + uriList.size());

		return System.currentTimeMillis();
	}
}
