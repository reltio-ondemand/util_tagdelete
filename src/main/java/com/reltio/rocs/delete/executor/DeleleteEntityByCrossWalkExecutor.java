/**
 * 
 */
package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Entites;
import com.reltio.rocs.delete.dto.Entity;

/**
 * 
 * @author spatil
 */
public class DeleleteEntityByCrossWalkExecutor implements Callable<Long> {

	private BufferedWriter writer;
	private ReltioAPIService restApi;
	private List<String> sourceInfo;
	private String apiUrl;
	private String dili;
	private Boolean load=false;

	public DeleleteEntityByCrossWalkExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl,List<String> sourceInfo, String dili) {

		this.apiUrl = apiUrl;
		this.writer = writer;
		this.sourceInfo = sourceInfo;
		this.restApi = restApi;
		this.dili = dili;
	}
	
	public DeleleteEntityByCrossWalkExecutor(BufferedWriter writer, ReltioAPIService restApi, String apiUrl,List<String> sourceInfo, String dili,Boolean load) {
		
		this.apiUrl = apiUrl;
		this.writer = writer;
		this.sourceInfo = sourceInfo;
		this.restApi = restApi;
		this.dili = dili;
		this.load = load;
	}

	@Override
	public Long call() throws Exception {
		
		for (String uri : sourceInfo) {
			String[] arr=uri.split("\\"+dili,-1);
			String body="[{\"value\":\"xxxxxx\",\"type\": \"configuration/sources/yyyyyy\"}]";
			body=body.replaceAll("xxxxxx", arr[1]).replaceAll("yyyyyy", arr[0]);
			Gson gson = new Gson();
			
			if(load){
				try {
					String restResponse = restApi.post(apiUrl + "/entities/_byCrosswalks?select=uri", body);
					Entites[] res = gson.fromJson(restResponse, Entites[].class);
					for (Entites entry : res) {
						Entity entity = entry.getEntitiy();
						try {
							if(arr.length>=3&&arr[2]!=null&&!arr[2].equals("")){
								String tag="[\""+arr[2]+"\"]";
								String addRessponse = restApi.post(apiUrl + entity.getUri()+"/tags", tag);
								writer.write(entity.getUri() + " -- " + addRessponse+"\n");
								writer.flush();
							}
							
						} catch (Exception e) {
							System.out.println("failed to add " + entity.getUri() + e.getMessage());
						}
					}
					System.out.println(Arrays.toString(res));
				} catch (Exception e) {
					System.out.println(e.getCause());
				}
			}else{
				try {
					String restResponse = restApi.post(apiUrl + "/entities/_byCrosswalks?select=uri,tags", body);
					Entites[] res = gson.fromJson(restResponse, Entites[].class);
					for (Entites entry : res) {
						Entity entity = entry.getEntitiy();
						try {
							for (String tag :entity.getTags()) {
								String tagvalue=URLEncoder.encode(tag, "UTF-8");
								
								if(arr.length>=3&&arr[2]!=null&&!arr[2].equals("")){
									if(tag.equalsIgnoreCase(arr[2])){
										String deleteRessponse = restApi.delete(apiUrl + entity.getUri()+"/tags?element="+tagvalue, "");
										writer.write(tag + " -- " + deleteRessponse+"\n");
										writer.flush();
									}
								}else{
									String deleteRessponse = restApi.delete(apiUrl + entity.getUri()+"/tags?element="+tagvalue, "");
									writer.write(tag + " -- " + deleteRessponse+"\n");
									writer.flush();
								}
							}
							
						} catch (Exception e) {
							System.out.println("failed to delete " + entity.getUri() + e.getMessage());
						}
					}
					System.out.println(Arrays.toString(res));
				} catch (Exception e) {
					System.out.println(e.getCause());
				}
			}
			
		}
		
		return System.currentTimeMillis();
	}

}
