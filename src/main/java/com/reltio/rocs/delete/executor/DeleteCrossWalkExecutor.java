package com.reltio.rocs.delete.executor;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Uri;

/**
 * This callable class deletes cross walk in entity 
 **/
public class DeleteCrossWalkExecutor implements Callable<Long> {

	private BufferedWriter writer;
	
	private List<Uri> uris = Collections.emptyList();
	
	private ReltioAPIService restApi;
	
	private String apiUrl;

	public DeleteCrossWalkExecutor(BufferedWriter writer, List<Uri> uris, ReltioAPIService restApi, String apiUrl) {
	
		this.writer = writer;
		this.uris = uris;
		this.restApi = restApi;
		this.apiUrl = apiUrl;
	}

	@Override
	public Long call() throws Exception {

		for (Uri uri : uris) {
			System.out.println("deleting " + apiUrl + uri.getUri());
			String resp = restApi.get(apiUrl + uri.getUri());
			writer.write(uri.getUri() + "--" + resp);
			writer.flush();
			writer.newLine();
		}
		
		return System.currentTimeMillis();
	}
}