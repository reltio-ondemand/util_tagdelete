# Quick Start 

##Building

The path to build the Tag Utility is 
util_tagdelete\src\main\java\com\reltio\rocs\delete\main\Delete.java

##Dependencies 

1. reltio-cst-core-1.4.9
3. gson-2.8.0

##Parameters and Properties File Example

```
#!paintext

#Common Properties
TENANT_ID=swyQZ6vj3SlyfaM
ENVIRONMENT_URL=sndbx.reltio.com
USERNAME=**************@reltio.com
PASSWORD=password
CLIENT_CREDENTIALS=
AUTH_URL=https://auth.reltio.com/oauth/token
HTTP_PROXY_HOST=
HTTP_PROXY_PORT=

#Tool Specific Properties
OUTPUT_FILE=output.txt
DELIMITER=|


#Available actions [DeleteAll, DeleteSingleTag, filter, uris, crosswalks, Addtag]
ACTION=DeleteAll

#When ACTION=DeleteAll, use below input
INPUT=exists(tags)

#When ACTION=DeleteSingleTag, use below input
#INPUT=equals(tags,'Training')

#When ACTION=sourcesystem or filter, use below input and don't remove exists(tags) logic for any filter
#INPUT=equals(type,'configuration/entityTypes/HCP') and startsWith(attributes.FirstName,'John') and exists(tags)

#When ACTION=Uris, use below input
#INPUT=C:\\Users\\Desktop\\uriList.txt

#When ACTION=crosswalks, use below inputs
#INPUT=C:\\Users\\Globalsoft\\Desktop\\crosswalkList.txt
#file should be of below format,
#SourceName|SourceValue|Tags
#IMS|123465798798|training


#When ACTION=Addtag,Only this param will allow user to add the tags into a profile, use below inputs
INPUT=C:\\Users\\Desktop\\inputFileList.txt
#file should be of below format,
#SourceName|SourceValue|Tags
#IMS|123465798798|training


#delimiter works only In cases of ACTION=crosswalks, Addtag


```

##Input File Example - for ACTION = ADDTag

```
#!paintext
SourceName|SOurceValue|Tags
IMS|123465798798|training
MDS|798230092367|training
IMS|789572502642|Testing

```

##Input File Example - for ACTION = crosswalks

```
#!paintext
SourceName|CrossWalkValue|Tags
IMS|123465798798|training
MDS|798230092367|training
IMS|789572502642|Testing

```

##Input File Example - for ACTION=Uris

```
#!paintext
EntityUri
entities\huie73k
entities\yuie98u


```

##Executing

Command to start the utility.
```
#!plaintext

java -jar reltio-util-bulkDeleteTag-{{version}}.jar configuration.properties > $logfilepath$

Please use the updated vesion from the bitbucket.

```