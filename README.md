
#TAG UTILITY

## Description
The Tag Utility is the tool developed for creation and deletion of Tags for entities. This utility deletes the Tags associated to an entity based on entity and source system. The ‘Tag’ and ‘Roles’ are default attributes for every entity. Using this utility jar we can create and delete tags.

##Change Log

```
#!plaintext

Last Update Date: 30/09/2019
Version: 1.8.1
Description:Updated the utility version to three digit,JAR name standardization.
CLIENT_CREDENTIALS = This property used for to get the access token using client_credentials grant type. The value for this property can be obtained by encoding the client name and secret separated by colon in Base64 format. (clientname:clientsecret)


Last Update Date: 27/06/2019
Version: 1.8
Description:Utility cleanup, removed older jars,package standardization.
And proxy change

Last Update Date: 13/03/2019
Version: 1.0.7
Description:
1) Clear validation message when properties are missing
2) Encryption of password in properties file
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util_tagdelete/src/064e4897750f09451e33c0401b8888f9158e7421/QuickStart.md?at=master&fileviewer=file-view-default).


